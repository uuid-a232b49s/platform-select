cmake_minimum_required(VERSION 3.14)
cmake_policy(VERSION 3.14)

include_guard(GLOBAL)


include("${CMAKE_CURRENT_LIST_DIR}/util.cmake")


# # #
# platform_test_success(priority)
#
# Invoked from test context upon success
#
# Args:
#    - [0] <number> priority : if multiple tests succeed, the test tht reported the highest priority will be selected as the final implementation
function(platform_test_success priority)
    set(_platform_test_pass TRUE PARENT_SCOPE)
    set(_platform_test_prio "${priority}" PARENT_SCOPE)
endfunction()

# # #
# platform_test_fail()
#
# Invoked from test context upon failure
function(platform_test_fail)
    set(_platform_test_pass FALSE PARENT_SCOPE)
endfunction()

# # #
# platform_is_test(out)
#
# Invoked from the test context to check whether the module is running in test mode
#
# Args:
#   - [0] <var name> out : sets the specified variable to TRUE (if currently running a test) or FALSE
macro(platform_is_test out)
    if(DEFINED platform_mode)
        if("${platform_mode}" STREQUAL "test")
            set(${out} TRUE)
        else()
            set(${out} FALSE)
        endif()
    endif()
endmacro()

# # #
# platform_is_impl(out)
#
# Invoked from the test context to check whether the module is running in implementation mode
# Args:
#   - [0] <var name> out : sets the specified variable to TRUE (if currently running in implementation mode) or FALSE
macro(platform_is_impl out)
    if(DEFINED platform_mode)
        if("${platform_mode}" STREQUAL "impl")
            set(${out} TRUE)
        else()
            set(${out} FALSE)
        endif()
    endif()
endmacro()



# # #
# platform_load_component_dir(component_dir required silent)
#
# Scans the specified component dir (component_dir) for any cmake modules (non recursive)
# Any module found is treated as a potential implementation for the component
# First each found module is invoked in test mode
# After that the test with the highest priority is chosen as the implementation of this component and invoked in implementation mode
# If no modules success then the component is deemed not implementable, and in case it is required an error is gennerated
#
# Args:
#   - [0] <path> component_dir : target directory to scan for implementations
#   - [1] <bool> required : whether this component is required - not implemented components will generate an error if this flag is true
#   - [2] <bool> silent : whether to skip status logs
macro(platform_load_component_dir component_dir required silent)
    _platform_load_component_dir("${component_dir}" "${required}" "${silent}" _platform_load_component_dir_impl)
    if(DEFINED _platform_load_component_dir_impl)
        set(platform_mode "impl")
        include("${_platform_load_component_dir_impl}")
    endif()
endmacro()

# # #
# platform_load_platform_dir(platform_dir all_comps_required silent)
#
# Scans the specified platform dir (platform_dir) for any subdirs (non recursive) and treats any that are found as component dirs
# After the scan, each found directory is loaded as a component dir (see platform_load_component_dir)
#
# Args:
#   - [0] <path> platform_dir : target directory to scan for components
#   - [1] <bool> all_comps_required : whether each component must succeed to load
#   - [2] <bool> silent : whether to disable cmake status messages
macro(platform_load_platform_dir platform_dir all_comps_required silent)
    file(GLOB _platform_load_platform_dir_comp_dirs LIST_DIRECTORIES TRUE "${platform_dir}/*")
    foreach(_platform_load_platform_dir_comp_dir IN LISTS _platform_load_platform_dir_comp_dirs)
        if(IS_DIRECTORY "${_platform_load_platform_dir_comp_dir}")
            platform_load_component_dir("${_platform_load_platform_dir_comp_dir}" "${all_comps_required}" "${silent}")
        endif()
    endforeach()
endmacro()



function(_platform_load_component_dir component_dir required silent out_impl)
    get_filename_component(_comp_dir_name "${component_dir}" NAME)
    file(GLOB _impl_files LIST_DIRECTORIES FALSE "${component_dir}/*.cmake")

    set(_impl_count 0)
 
    foreach(_impl_file IN LISTS _impl_files)
        _platform_invoke_test("${_impl_file}")
        if(${_pass})
            get_filename_component(_impl_name_${_impl_count} "${_impl_file}" NAME_WE)
            set(_impl_file_${_impl_count} "${_impl_file}")
            set(_impl_prio_${_impl_count} ${_prio})
            math(EXPR _impl_count "${_impl_count}+1")
        endif()
    endforeach()

    if(${_impl_count} EQUAL 0)
        if(${required})
            set(_mode SEND_ERROR)
        else()
            set(_mode STATUS)
        endif()
        if(NOT ${silent})
            message(${_mode} "Failed to find platform implementation for ${_comp_dir_name}")
        endif()
        unset(${out_impl} PARENT_SCOPE)
        return()
    endif()

    set(_selected_idx 0)

    math(EXPR _impl_count_m1 "${_impl_count}-1")
    if(1 LESS ${_impl_count_m1})
        foreach(_i RANGE 1 ${_impl_count_m1})
            if(${_impl_prio_${_i}} GREATER ${_impl_prio_${_selected_idx}})
                set(_selected_idx ${_i})
            endif()
        endforeach()
    endif()
    
    if(NOT ${silent})
        message(STATUS "Found implementation for ${_comp_dir_name}: ${_impl_name_${_selected_idx}}")
    endif()

    set(${out_impl} "${_impl_file_${_selected_idx}}" PARENT_SCOPE)
endfunction()

function(_platform_invoke_test file)
    unset(_platform_test_pass)
    unset(_platform_test_prio)
    set(platform_mode "test")
    include("${file}")
    if( (DEFINED _platform_test_pass) AND ("${_platform_test_pass}" STREQUAL TRUE) )
        set(_pass TRUE PARENT_SCOPE)
        if(DEFINED _platform_test_prio)
            set(_prio "${_platform_test_prio}" PARENT_SCOPE)
        else()
            set(_prio 0 PARENT_SCOPE)
        endif()
    else()
        set(_pass FALSE PARENT_SCOPE)
    endif()
endfunction()
