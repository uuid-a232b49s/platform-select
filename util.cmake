cmake_minimum_required(VERSION 3.14)
cmake_policy(VERSION 3.14)

include_guard(GLOBAL)



# # #
# platform_listfile_to_id(out)
# Creates a unique text id based on the path to the currently executing list file
# Args:
#   - [0] <var name> out : sets the specified variable to the generated value
macro(platform_listfile_to_id out)
    file(RELATIVE_PATH ${out} "${PROJECT_SOURCE_DIR}" "${CMAKE_CURRENT_LIST_FILE}")
    string(MAKE_C_IDENTIFIER "_id_${PROJECT_NAME}_${${out}}" ${out})
endmacro()
